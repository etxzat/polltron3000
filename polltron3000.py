#!/usr/bin/env python3
from functools import reduce
import sys
import requests
import argparse
from argparse import BooleanOptionalAction
from requests.compat import urljoin
from bs4 import BeautifulSoup

def dump(r, name):
    with open(name, 'w') as f:
        f.write(r.text)

def compare_all(xs):
    return reduce(lambda a, b: a if a == b else None, xs[1:], xs[0]) is not None

def gen_datetime_list(dates, times):
    return ', '.join([f'{date} {time}' for date in dates for time in times])


class PollSession:
    def __init__(self, url, dates, times):
        self.url = url
        self.s = requests.Session()
        self.s.headers.update({'user-agent': 'Mozilla/5.0 (X11; Linux x86_64)'})
        self.dates = dates
        self.times = times

    def get(self, *args, **kwargs):
        self.r = self.s.get(*args, **kwargs)
        if self.r.status_code != 200:
            print(self.r)
            self.die()
        self.pagehtml = BeautifulSoup(self.r.text, features='html5lib')
        self.check_errors()
        self.s.headers.update({'Referer': str(self.r.request.url)})
        self.url = self.r.url

    # TODO: fix argument passing?
    def post(self, form_id, payload, *args, **kwargs):
        if type(form_id) == str:
            form = self.pagehtml.find('form', {'id': form_id})
        else:
            # set form to previously parsed html tag
            form = form_id
        action = form['action']
        self.url = urljoin(self.url, action)
        request_token = form.find('input',
                                  attrs={'name': 'csrfmiddlewaretoken'})['value']
        payload['csrfmiddlewaretoken'] = request_token
        self.r = self.s.post(self.url, data=payload, **kwargs)
        if self.r.status_code != 200:
            print(self.r)
            self.die()
        self.pagehtml = BeautifulSoup(self.r.text, features='html5lib')
        self.check_errors()
        self.s.headers.update({'Referer': str(self.r.request.url)})
        self.url = self.r.url

    def check_errors(self):
        errorlisthtml = self.pagehtml.find('ul', {'class': 'errorlist'})
        if errorlisthtml is not None:  # there was an error
            print('Errors creating the poll occured:')
            for error in errorlisthtml.find('li'):
                print(f'\t- {error}')
            self.die()

    def die(poll_session):
        print('Fatal error, aborting. See "error.html" for details', file=sys.stderr)
        dump(poll_session.r, 'error.html')
        exit(1)

    def init_connection(self):
        self.get(self.url)

    def new_poll(self, name, poll_type, url='', **kwargs):
        bool_params = ['anonymous_allowed',
                       'one_vote_per_user',
                       'vote_all',
                       'public_listening',
                       'require_login',
                       'require_invitation',
                       'allow_unauthenticated_vote_changes',
                       ]
        text_params = ['description',
                       'due_date',
                       ]
        # TODO: default parameters from form
        payload = {bp: 'on' for bp in bool_params if bp in kwargs and kwargs[bp]}
        payload |= {tp: kwargs[tp] if tp in kwargs else '' for tp in text_params}
        payload['title'] = title
        payload['type'] = poll_type
        if len(url) > 0:
            payload['url'] = url
        else:
            payload['url'] = ''
            payload['random_slug'] = 'on'
        self.post('', payload)

    def set_dates(self):
        dateform = self.pagehtml.find('form', {'id': 'date-time-form'})
        date_input = dateform.find('input', {'id': 'id_dates'})
        date_key = date_input['name']
        payload = {
            date_key: ','.join(self.dates),
        }
        self.post('date-time-form', payload)

    def set_times(self):
        # find time key
        timeform = self.pagehtml.find('form', {'id': 'date-time-form'})
        time_input = timeform.find('input', {'id': 'id_times'})
        time_key = time_input['name']
        # find date key
        dateform = self.pagehtml.find('form', {'id': 'date-time-form'})
        date_input = dateform.find('input', {'id': 'id_dates'})
        date_key = date_input['name']
        date_val = date_input['value']
        payload = {
            date_key: date_val,
            time_key: ','.join(self.times),
        }
        self.post('date-time-form', payload)

    def select_slots(self):
        # find correct form
        # TODO solve this upstream by adding a unique id to the form
        forms = self.pagehtml.find_all('form')
        datetime_lists = [[i['value'] for i in f.find_all('input')
                           if 'name' in i.attrs and i['name'] == 'datetimes[]']
                          for f in forms]
        form_index = next(i for i, v in enumerate(datetime_lists) if len(v) > 0)
        form = forms[form_index]
        datetime_list = datetime_lists[form_index]
        payload = {
            'datetimes[]': datetime_list
        }
        self.post(form, payload)

if __name__ == '__main__':
    ap = argparse.ArgumentParser(description='Command-line client for the online poll and calendar tool BitPoll',
                                 prog='polltron3000')
    ap.add_argument('URL', nargs=1, help='URL to the BitPoll instance')
    ap.add_argument('TITLE', nargs=1, help='Title of the poll.')
    ap.add_argument('DESCRIPTION', nargs='?', help='Description for the poll.')
    ap.add_argument('-u', '--title-as-url', action='store_true',
                    help="Don't randomize the URL and use the title instead.")
    ap.add_argument('-d', '--dates', action='append', nargs='+',
                    help='List of dates. Format: YYYY-MM-DD')
    ap.add_argument('-t', '--times', action='append', nargs='+',
                    help='List of time-slots. Format: HH:MM')
    ap.add_argument('-p', '--public', action=BooleanOptionalAction, default=False,
                    help='Create a public poll.')
    ap.add_argument('--allow-anonymous', action=BooleanOptionalAction, default=True,
                    help='Allow anonymous votes.')
    ap.add_argument('--allow-empty-choices', action=BooleanOptionalAction, default=False,
                    help='Allow users to skip choices.')
    ap.add_argument('--allow-unauthenticated-vote-changes', action=BooleanOptionalAction, default=False,
                    help='Allow unauthenticated users to change votes.')
    ap.add_argument('--require-login', action=BooleanOptionalAction, default=False,
                    help='Require users to be logged in.')
    ap.add_argument('--require-invitation', action=BooleanOptionalAction, default=False,
                    help='Require an invitation to vote.')
    ap.add_argument('--one-vote-per-user', action=BooleanOptionalAction, default=True,
                    help='Allow only one vote per user.')

    args = ap.parse_args()
    url = args.URL[0]
    title = args.TITLE[0]
    randomize = False if args.title_as_url else True
    description = args.DESCRIPTION

    dates = args.dates[0]
    times = args.times[0]

    print(f'Creating poll: "{title}"')

    poll = PollSession(url, dates, times)
    poll.init_connection()
    poll.new_poll(title, 'datetime',
                  url='' if randomize else title,
                  description=description,
                  anonymous_allowed=args.allow_anonymous,
                  one_vote_per_user=args.one_vote_per_user,
                  vote_all=not args.allow_empty_choices,
                  public_listening=args.public,

                  require_login=args.require_login,
                  require_invitation=args.require_invitation,
                  allow_unauthenticated_vote_changes=args.allow_unauthenticated_vote_changes,
                  )

    poll.set_dates()
    poll.set_times()
    poll.select_slots()
    print(poll.url)
